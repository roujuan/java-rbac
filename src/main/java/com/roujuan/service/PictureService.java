package com.roujuan.service;

import com.roujuan.entity.Picture;
import com.roujuan.entity.PictureQuery;

import java.util.List;

public interface PictureService {
    List<Picture> getPictureList();
    List<Picture> getPictureListByQuery(PictureQuery pictureQuery);
    Integer getPictureListTotal(PictureQuery pictureQuery);
    Integer insertPicture(Picture picture);
    Integer updatePicture(Picture picture);
    Integer deletePicture(Integer id);
}
