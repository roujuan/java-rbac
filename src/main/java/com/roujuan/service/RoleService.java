package com.roujuan.service;

import com.roujuan.entity.Role;

import java.util.List;

public interface RoleService {
    List<Role> getRole();
}
