package com.roujuan.service.impl;

import com.roujuan.entity.Department;
import com.roujuan.entity.DepartmentQuery;
import com.roujuan.mapper.DepartmentMapper;
import com.roujuan.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentMapper departmentMapper;
    @Override
    public List<Department> getDepartmentList(){
        return departmentMapper.getDepartmentList();
    }
    @Override
    public List<Department> getDepartmentListByQuery(DepartmentQuery departmentQuery){
        departmentQuery.setPage((departmentQuery.getPage()-1)*departmentQuery.getLimit());
        return departmentMapper.getDepartmentListByQuery(departmentQuery);
    }

    @Override
    public Integer getDepartmentListTotal(DepartmentQuery departmentQuery){
        return departmentMapper.getDepartmentListTotal(departmentQuery);
    }
    @Override
    public Integer deleteDepartment(Integer id){
        return departmentMapper.deleteDepartment(id);
    }

    @Override
    public Integer insertDepartment(Department department){
        return departmentMapper.insertDepartment(department);
    }

    @Override
    public Integer updateDepartment(Department department){
        return departmentMapper.updateDepartment(department);
    };
}
