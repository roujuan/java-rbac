package com.roujuan.service.impl;

import com.roujuan.entity.Role;
import com.roujuan.mapper.RoleMapper;
import com.roujuan.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;
    @Override
    public  List<Role> getRole(){
        return roleMapper.getRole();
    };
}
