package com.roujuan.service.impl;

import com.roujuan.entity.Article;
import com.roujuan.entity.ArticleQuery;
import com.roujuan.mapper.ArticleMapper;
import com.roujuan.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleMapper articleMapper;
    @Override
    public Integer insertArticle(Article article){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = null;
        try {
            date = simpleDateFormat.parse(article.getTimestamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long ts = date.getTime();
        article.setTimestamp(ts+"");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        article.setDisplay_time(sdf.format(new Date(Long.valueOf(ts)))+"");
        return articleMapper.insertArticle(article);
    }
    @Override
    public List<Article> getArticleList(ArticleQuery articleQuery){
        articleQuery.setPage((articleQuery.getPage()-1)*articleQuery.getLimit());
        return  articleMapper.getArticleList(articleQuery);
    }

    @Override
    public Integer getArticleListTotal(ArticleQuery articleQuery){
        return  articleMapper.getArticleListTotal(articleQuery);
    }

    @Override
    public Integer deleteArticleById(Integer id){
        return  articleMapper.deleteArticleById(id);
    }

    @Override
    public Integer updateArticleStatusById(Integer id,String status){
        return  articleMapper.updateArticleStatusById(id,status);
    }

    @Override
    public Integer updateArticleById(Article article){
        return  articleMapper.updateArticleById(article);
    }
}
