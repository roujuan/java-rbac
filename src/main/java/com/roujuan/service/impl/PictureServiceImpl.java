package com.roujuan.service.impl;

import com.roujuan.entity.Picture;
import com.roujuan.entity.PictureQuery;
import com.roujuan.mapper.PictureMapper;
import com.roujuan.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PictureServiceImpl implements PictureService {
    @Autowired
    private PictureMapper pictureMapper;
    @Override
    public List<Picture> getPictureList(){
        return pictureMapper.getPictureList();
    }
    @Override
    public List<Picture> getPictureListByQuery(PictureQuery pictureQuery){
        pictureQuery.setPage((pictureQuery.getPage()-1)*pictureQuery.getLimit());
        return pictureMapper.getPictureListByQuery(pictureQuery);
    }
    @Override
    public Integer getPictureListTotal(PictureQuery pictureQuery){
        return pictureMapper.getPictureListTotal(pictureQuery);
    }
    @Override
    public Integer insertPicture(Picture picture){
        return pictureMapper.insertPicture(picture);
    }
    @Override
    public  Integer updatePicture(Picture picture){
        return pictureMapper.updatePicture(picture);
    }
    @Override
    public  Integer deletePicture(Integer id){
        return pictureMapper.deletePicture(id);
    }
}
