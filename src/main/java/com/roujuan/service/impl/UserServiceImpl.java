package com.roujuan.service.impl;

import com.roujuan.entity.User;
import com.roujuan.entity.UserInfo;
import com.roujuan.entity.UserQuery;
import com.roujuan.mapper.UserMapper;
import com.roujuan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public User getUserByName(String username) {
        return userMapper.getUser(username);
    }
    @Override
    public UserInfo getUserInfoByToken(String token){
        return userMapper.getUserInfo(token);
    }
    @Override
    public Integer insertUser(User user){
        return userMapper.insertUser(user);
    }

    @Override
    public Integer insertUserInfo(UserInfo userInfo){
        return userMapper.insertUserInfo(userInfo);
    }

    @Override
    public List<UserInfo> getUserList(UserQuery userQuery){
        userQuery.setPage((userQuery.getPage()-1)*userQuery.getLimit());
        return userMapper.getUserList(userQuery);
    }
    @Override
    public Integer getUserListTotal(UserQuery userQuery){
        return userMapper.getUserListTotal(userQuery);
    }
    @Override
    public Integer deleteUserById(Integer uid){
        return userMapper.deleteUserById(uid);
    };

    @Override
    public Integer editUser(User user){
        return userMapper.editUser(user);
    }
    @Override
    public Integer editUserInfo(UserInfo userInfo){
        return userMapper.editUserInfo(userInfo);
    }
}
