package com.roujuan.service;

import com.roujuan.entity.Department;
import com.roujuan.entity.DepartmentQuery;

import java.util.List;

public interface DepartmentService {
    List<Department> getDepartmentList();
    List<Department> getDepartmentListByQuery(DepartmentQuery departmentQuery);
    Integer getDepartmentListTotal(DepartmentQuery departmentQuery);
    Integer deleteDepartment(Integer id);
    Integer insertDepartment(Department department);
    Integer updateDepartment(Department department);
}
