package com.roujuan.service;

import com.roujuan.entity.Article;
import com.roujuan.entity.ArticleQuery;

import java.util.List;

public interface ArticleService {
    Integer insertArticle(Article article);
    List<Article> getArticleList(ArticleQuery articleQuery);
    Integer getArticleListTotal(ArticleQuery articleQuery);
    Integer deleteArticleById(Integer id);
    Integer updateArticleStatusById(Integer id,String status);
    Integer updateArticleById(Article article);
}
