package com.roujuan.service;

import com.roujuan.entity.User;
import com.roujuan.entity.UserInfo;
import com.roujuan.entity.UserQuery;

import java.util.List;

public interface UserService {
//    验证成功返回User对象，否则null或抛出错误
    User getUserByName(String username);
    UserInfo getUserInfoByToken(String token);
    Integer insertUser(User user);
    Integer insertUserInfo(UserInfo userInfo);
    List<UserInfo> getUserList(UserQuery userQuery);
    Integer getUserListTotal(UserQuery userQuery);
    Integer deleteUserById(Integer uid);
    Integer editUser(User user);
    Integer editUserInfo(UserInfo userInfo);
}
