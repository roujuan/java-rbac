package com.roujuan.entity;

public class Data {
    private String token;
    private String[] roles;
    private String introduction;
    private String avatar;
    private String name;
    public Data(String token) {
        this.token = token;
    }

    public Data(String[] roles, String introduction, String avatar, String name) {
        this.roles = roles;
        this.introduction = introduction;
        this.avatar = avatar;
        this.name = name;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public String[] getRoles() {
        return roles;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Data{" +
                "token='" + token + '\'' +
                ", roles='" + roles + '\'' +
                ", introduction='" + introduction + '\'' +
                ", avatar='" + avatar + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
