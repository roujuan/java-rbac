package com.roujuan.entity;

import java.sql.Timestamp;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;

public class Article {
    private Integer id;
    private String author;
    private Integer importance;
    private String remark;
    private String status;
    private String title;
    private String type;
    private String timestamp;
    private String display_time;

    public void setDisplay_time(String display_time) {
        this.display_time = display_time;
    }

    public String getDisplay_time() {
        return display_time;
    }

    public void setTimestamp(String timestamp) { this.timestamp = timestamp; }

    public String getTimestamp() {
        return timestamp;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setImportance(Integer importance) {
        this.importance = importance;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public Integer getImportance() {
        return importance;
    }

    public String getRemark() {
        return remark;
    }

    public String getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", importance=" + importance +
                ", remark='" + remark + '\'' +
                ", status='" + status + '\'' +
                ", title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", display_time='" + display_time + '\'' +
                '}';
    }
}
