package com.roujuan.entity;

import java.util.List;

public class PictureList {
    private Integer total;
    private List<Picture> items;

    public PictureList(Integer total, List<Picture> items) {
        this.total = total;
        this.items = items;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public void setItems(List<Picture> items) {
        this.items = items;
    }

    public Integer getTotal() {
        return total;
    }

    public List<Picture> getItems() {
        return items;
    }
}
