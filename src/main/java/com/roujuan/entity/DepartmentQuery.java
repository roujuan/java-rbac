package com.roujuan.entity;

public class DepartmentQuery {
    private int page;
    private int limit;
    private String department;
    private String sort;

    @Override
    public String toString() {
        return "DepartmentQuery{" +
                "page=" + page +
                ", limit=" + limit +
                ", department='" + department + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }

    public void setPage(int page) {

        this.page = page;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public int getPage() {
        return page;
    }

    public int getLimit() {
        return limit;
    }

    public String getDepartment() {
        return department;
    }

    public String getSort() {
        return sort;
    }
}
