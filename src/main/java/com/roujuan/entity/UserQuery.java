package com.roujuan.entity;

public class UserQuery {
    private int page;
    private int limit;
    private String roles;
    private String name;
    private String sort;

    @Override
    public String toString() {
        return "UserQuery{" +
                "page=" + page +
                ", limit=" + limit +
                ", roles='" + roles + '\'' +
                ", name='" + name + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public int getPage() {
        return page;
    }

    public int getLimit() {
        return limit;
    }

    public String getRoles() {
        return roles;
    }

    public String getName() {
        return name;
    }

    public String getSort() {
        return sort;
    }
}
