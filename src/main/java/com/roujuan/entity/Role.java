package com.roujuan.entity;

public class Role {
    private String roleType;
    private String roleName;
    private String description;

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoleType() {
        return roleType;
    }

    public String getRoleName() {
        return roleName;
    }

    public String getDescription() {
        return description;
    }
}
