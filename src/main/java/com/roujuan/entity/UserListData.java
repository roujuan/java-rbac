package com.roujuan.entity;

import java.util.List;

public class UserListData {
    private Integer total;
    private List<UserInfo> items;

    @Override
    public String toString() {
        return "UserListData{" +
                "total=" + total +
                ", items=" + items +
                '}';
    }

    public UserListData(Integer total, List<UserInfo> items) {
        this.total = total;
        this.items = items;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public void setItems(List<UserInfo> items) {
        this.items = items;
    }

    public Integer getTotal() {
        return total;
    }

    public List<UserInfo> getItems() {
        return items;
    }
}
