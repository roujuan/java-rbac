package com.roujuan.entity;

public class ArticleQuery {
   private int page;
   private int limit;
   private String importance;
   private String title;
   private String type;
   private String sort;

    @Override
    public String toString() {
        return "ArticleQuery{" +
                "page=" + page +
                ", limit=" + limit +
                ", importance='" + importance + '\'' +
                ", title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setImportance(String importance) {
        this.importance = importance;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public int getPage() {
        return page;
    }

    public int getLimit() {
        return limit;
    }

    public String getImportance() {
        return importance;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public String getSort() {
        return sort;
    }
}
