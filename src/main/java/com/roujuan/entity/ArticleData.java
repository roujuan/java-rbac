package com.roujuan.entity;

import java.util.List;

public class ArticleData {
    private Integer total;
    private List<Article> items;

    public ArticleData(Integer total, List<Article> items) {
        this.total = total;
        this.items = items;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public void setItems(List<Article> items) {
        this.items = items;
    }

    public Integer getTotal() {
        return total;
    }

    public List<Article> getItems() {
        return items;
    }
}
