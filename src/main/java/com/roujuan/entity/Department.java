package com.roujuan.entity;

public class Department {
    private Integer id;
    private String department;

    @Override
    public String toString() {
        return "Department{" +
                "departmentId=" + id +
                ", department='" + department + '\'' +
                '}';
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Integer getId() {
        return id;
    }

    public String getDepartment() {
        return department;
    }
}
