package com.roujuan.entity;

public class Picture {
    private Integer id;
    private String url;
    private String display_name;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public Integer getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getDisplay_name() {
        return display_name;
    }
}
