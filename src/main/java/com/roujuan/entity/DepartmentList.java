package com.roujuan.entity;

import java.util.List;

public class DepartmentList {
    private Integer total;
    private List<Department> items;

    @Override
    public String toString() {
        return "DepartmentList{" +
                "total=" + total +
                ", items=" + items +
                '}';
    }

    public DepartmentList(Integer total, List<Department> items) {
        this.total = total;
        this.items = items;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public void setItems(List<Department> items) {
        this.items = items;
    }

    public Integer getTotal() {
        return total;
    }

    public List<Department> getItems() {
        return items;
    }
}
