package com.roujuan.entity;

public class PictureQuery {
    private int page;
    private int limit;
    private String display_name;
    private String sort;

    @Override
    public String toString() {
        return "PictureQuery{" +
                "page=" + page +
                ", limit=" + limit +
                ", display_name='" + display_name + '\'' +
                ", sort='" + sort + '\'' +
                '}';
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public int getPage() {
        return page;
    }

    public int getLimit() {
        return limit;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public String getSort() {
        return sort;
    }
}
