package com.roujuan.mapper;

import com.roujuan.entity.User;
import com.roujuan.entity.UserInfo;
import com.roujuan.entity.UserQuery;

import java.util.List;

public interface UserMapper {
    User getUser(String username);
    UserInfo getUserInfo(String token);
    Integer insertUser(User user);
    Integer insertUserInfo(UserInfo userInfo);
    List<UserInfo> getUserList(UserQuery userQuery);
    Integer getUserListTotal(UserQuery userQuery);
    Integer deleteUserById(Integer uid);
    Integer editUser(User user);
    Integer editUserInfo(UserInfo userInfo);
}
