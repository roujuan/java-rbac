package com.roujuan.mapper;

import com.roujuan.entity.Article;
import com.roujuan.entity.ArticleQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleMapper {
    Integer insertArticle(Article article);
    List<Article> getArticleList(ArticleQuery articleQuery);
    Integer getArticleListTotal(ArticleQuery articleQuery);
    Integer deleteArticleById(Integer id);
    Integer updateArticleStatusById(@Param("id") Integer id, @Param("status") String status);
    Integer updateArticleById(Article article);
}
