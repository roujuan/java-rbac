package com.roujuan.mapper;

import com.roujuan.entity.Role;

import java.util.List;

public interface RoleMapper {
    List<Role> getRole();
}
