package com.roujuan.controller;

import com.alibaba.fastjson.JSONObject;
import com.roujuan.entity.Article;
import com.roujuan.entity.ArticleData;
import com.roujuan.entity.ArticleQuery;
import com.roujuan.entity.UserInfo;
import com.roujuan.service.ArticleService;
import com.roujuan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private UserService userService;
    @RequestMapping("/article/create")
    @ResponseBody
    public String insertArticle(Article article, @RequestHeader("X-Token") String token) {
        // 判断token是否合法/存在，成功则添加数据
        try {
            UserInfo temp = userService.getUserInfoByToken(token);
            if (temp != null) {
                Integer result = articleService.insertArticle(article);
                if(result==1) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("code", 20000);
                    jsonObject.put("data", "success");
                    return JSONObject.toJSONString(jsonObject);
                }else{
                    return "failed";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    return "failed";
    }

    @RequestMapping("/article/list")
    @ResponseBody
    public String getArticleList(ArticleQuery articleQuery){
        List<Article> articleList =  articleService.getArticleList(articleQuery);
        Integer total = articleService.getArticleListTotal(articleQuery);
        ArticleData articleData = new ArticleData(total,articleList);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 20000);
        jsonObject.put("data",articleData);
        return JSONObject.toJSONString(jsonObject);
    }

    @RequestMapping("/article/delete")
    @ResponseBody
    public String deleteArticleById(@RequestHeader("X-Token") String token,Integer id){
        // 判断token是否合法/存在，成功则添加数据
        try {
            UserInfo temp = userService.getUserInfoByToken(token);
            if (temp != null) {
                Integer result = articleService.deleteArticleById(id);
                if(result==1) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("code", 20000);
                    jsonObject.put("data", "success");
                    return JSONObject.toJSONString(jsonObject);
                }else{
                    return "failed";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "failed";
    }

    // 更新文章状态
    @RequestMapping("/article/updateArticleStatus")
    @ResponseBody
    public String updateArticleStatusById(@RequestHeader("X-Token") String token,Integer id,String status){
//        System.out.println(id);
//        System.out.println(status);
        // 判断token是否合法/存在，成功则添加数据
        try {
            UserInfo temp = userService.getUserInfoByToken(token);
            if (temp != null) {
                Integer result = articleService.updateArticleStatusById(id,status);
                if(result==1) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("code", 20000);
                    jsonObject.put("data", "success");
                    return JSONObject.toJSONString(jsonObject);
                }else{
                    return "failed";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "failed";
    }

    // 更新文章
    @RequestMapping("/article/update")
    @ResponseBody
    public String updateArticleById(@RequestHeader("X-Token") String token,Article article){
        System.out.println(article);
        // 判断token是否合法/存在，成功则添加数据
        try {
            UserInfo temp = userService.getUserInfoByToken(token);
            if (temp != null) {
                Integer result = articleService.updateArticleById(article);
                if(result==1) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("code", 20000);
                    jsonObject.put("data", "success");
                    return JSONObject.toJSONString(jsonObject);
                }else{
                    return "failed";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "failed";
    }


}
