package com.roujuan.controller;

import com.alibaba.fastjson.JSONObject;
import com.roujuan.entity.Role;
import com.roujuan.entity.UserInfo;
import com.roujuan.service.RoleService;
import com.roujuan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class RoleController {
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserService userService;
    @RequestMapping("/roles")
    @ResponseBody
    public String getRoles(@RequestHeader("X-Token") String token){
        try {
            UserInfo temp = userService.getUserInfoByToken(token);
            if (temp != null) {
                List<Role> roleList = roleService.getRole();

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("code", 20000);
                    jsonObject.put("data", roleList);
                    return JSONObject.toJSONString(jsonObject);
            }else return "failed";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "failed";
    }
}
