package com.roujuan.controller;

import com.alibaba.fastjson.JSONObject;
import com.roujuan.entity.*;
import com.roujuan.mapper.PictureMapper;
import com.roujuan.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class PictureController {
    @Autowired
    private PictureService pictureService;

    @RequestMapping("/picture/list")
    @ResponseBody
    public String getPictureList(){
        List<Picture> pictureList =  pictureService.getPictureList();
        Integer total = pictureList.size();
        PictureList pictureList1 = new PictureList(total,pictureList);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 20000);
        jsonObject.put("data",pictureList1);
        return JSONObject.toJSONString(jsonObject);
    }

    @RequestMapping("/picture/queryList")
    @ResponseBody
    public  String getPictureListByQuery(PictureQuery pictureQuery){
        List<Picture> pictureList = pictureService.getPictureListByQuery(pictureQuery);
        Integer total = pictureService.getPictureListTotal(pictureQuery);
        PictureList items = new PictureList(total,pictureList);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 20000);
        jsonObject.put("data",items);
        return JSONObject.toJSONString(jsonObject);
    }

    @RequestMapping("/picture/create")
    @ResponseBody
    public  String insertPicture(Picture picture){
        Integer result = pictureService.insertPicture(picture);
        if(result==1){
            Integer id = picture.getId();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", 20000);
            jsonObject.put("data",id);
            return JSONObject.toJSONString(jsonObject);
        }
        return "failed";
    }

    @RequestMapping("/picture/update")
    @ResponseBody
    public  String updatePicture(Picture picture){
        Integer result = pictureService.updatePicture(picture);
        if(result==1){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", 20000);
            jsonObject.put("data","success");
            return JSONObject.toJSONString(jsonObject);
        }
        return "failed";
    }

    @RequestMapping("/picture/delete")
    @ResponseBody
    public String deletePicture(Integer id){
        Integer result = pictureService.deletePicture(id);
        if(result==1){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", 20000);
            jsonObject.put("data","success");
            return JSONObject.toJSONString(jsonObject);
        }
        return "failed";
    }
}
