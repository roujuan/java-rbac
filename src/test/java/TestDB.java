import com.roujuan.entity.Article;
import com.roujuan.entity.User;
import com.roujuan.mapper.ArticleMapper;
import com.roujuan.mapper.UserMapper;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TestDB {
    @Test
    public void test(){
        ApplicationContext ap=new ClassPathXmlApplicationContext("spring-mybatis.xml");
        UserMapper bean = ap.getBean(UserMapper.class);
        User user = bean.getUser("admin");
        System.out.println(user);
    }

    @Test
    public void test2(){
        ApplicationContext ap=new ClassPathXmlApplicationContext("spring-mybatis.xml");
       ArticleMapper bean = ap.getBean(ArticleMapper.class);
        Article article = new Article();
        article.setAuthor("翁");
        article.setId(10);
        article.setImportance(3);
        article.setRemark("test");
        article.setStatus("published");
        article.setTimestamp("2021-02-19T16:46:21.000Z");
        article.setTitle("TEST");
        article.setType("CN");
      int x =  bean.insertArticle(article);
        System.out.println(x);

    }
    @Test
    public void test3() throws ParseException {
//        String tempTime = time.replace("Z", " UTC");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//        Date d = sdf.parse("2021-02-12T16:52:16.449Z");
//        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String str= sdf1.format(d);
//        new Date("2018/09/09 12:30:22").getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = simpleDateFormat.parse("2021-02-12T16:52:16.449Z");
        long ts = date.getTime();
        System.out.println(ts);
        }

}
